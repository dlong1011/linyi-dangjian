<!DOCTYPE html>

<html lang="zh-CN">
<head>
    <meta charset="utf-8" />
    <title>"两委"主干及村情档案系统</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <meta name="author" content="" />
    <link href="/content/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/content/zcms/public.css" rel="stylesheet" />
    <link href="/content/zcms/cont/index.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="/content/SidebarTransitions/css/component.css" />
    <link href="/content/wow/animate.min.css" rel="stylesheet" type="text/css"/>
    <script src="/content/Scripts/jquery-1.10.2.min.js"></script>
    <script src="/content/bootstrap-3.3.7/js/bootstrap.min.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
	  <script src="https://cdn.bootcss.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="/content/Scripts/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<div id="st-container" class="st-container">
	<div class="st-pusher">
	<div class="st-content">
		
	<a id="top"></a>
	<header class="visible-xs index-mob-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-8">
					<a href="index.htm"><img class="img-responsive small-logo" src="/content/zcms/index/images/logo-small.jpg" /></a>
				</div>
				<div id="st-trigger-effects" class="col-xs-4">
					<button class="navbar-toggle collapsed small-menu" data-effect="st-effect-7"><i class="glyphicon glyphicon-menu-hamburger"></i></button>
				</div>
			</div>
		</div>
	</header>
	<section class="container-fluid hidden-xs cont-header-big">
			<div class="row">
				<div class="col-sm-12 hidden-xs header-bg">
					<!-- <div class="cont-logo"><a><img src="content/zcms/index/images/logo.png"></a></div> -->
				</div>
			</div>
	</section>
	@yield('content')
<footer class="container-fluid footer-big hidden-xs">
	<div class="footer-con">
		<span>主办单位：中国共产党临猗县委员会组织部</span>
	</div>
	<div class="footer-con">
		<span>临猗县乡镇</span>
		<ul class="footer-county">
            @foreach($countys as $item)
            <li><a href ="{{URL::route('county-list', ['county' => $item['id']])}}">{{$item['name']}}</a></li>
            @endforeach
		</ul>
	</div>
</footer>
<footer class="visible-xs footer-small">
	<a href="index.htm"><li class="glyphicon glyphicon-home"></li><p>首页</p></a>
	<!-- <a href="tel:13111111111"><li class="glyphicon glyphicon-earphone"></li></a>
	<a href="mailto:123456@qq.com"><li class="glyphicon glyphicon-envelope"></li></a> -->
	<!--<a id="returnTop" class="back-top-small" onclick="" ><li class="glyphicon glyphicon-arrow-up"></li></a>-->
	<a id="returnTop" class="back-top-small" href="#top" ><li class="glyphicon glyphicon-arrow-up"></li></a>
</footer>
	</div>
		<nav class="st-menu st-effect-7" id="menu-7">
			<h2 class="icon icon-lab">主页</h2>
		</nav>
	</div>
</div>

</div>
    @section('script')
    <script src="/content/SidebarTransitions/js/classie.js"></script>
    <script src="/content/SidebarTransitions/js/sidebarEffects.js"></script>
    <script src="/content/wow/wow.min.js"></script>
    <script src="/content/zcms/index/index.js"></script>
    @show
    <script>
        if (!(/msie [6|7|8|9]/i.test(navigator.userAgent))){
            new WOW().init();
        };
    </script>
</body>
</html>
