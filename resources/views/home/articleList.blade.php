@extends('master')

@section('content')

<section class="container cont-main">
		<ul class="breadcrumb">
			<li><a href="/">首页</a></li>
			<li><a href="{{URL::route('county-list', ['county' => $menu['town']['id']])}}">{{$menu['town']['name']}}</a></li>
			<li><a href="{{URL::route('county-index', ['county' => $menu['village']['id']])}}">{{$menu['village']['name']}}</a></li>
			<!-- <li class="active"></li> -->
		</ul>
		<div class="row">
			  <div class="col-sm-12 col-xs-12 cont-at-text">
			  	  <h1>&nbsp; {{$type}}</h1>
			  </div>
		</div>
	</section>
	<section class="container cont-main cont-main-list">
		
		<div class="row">
			<div class="col-sm-12 col-md-8 cont-left wow bounceInLeft">
				<ul class="cont-news">
					@foreach ($list as $item)
					<li class="row">
						<div class="col-xs-10" style="height:30px;line-height:30px;">
							<h4><a href="{{URL::route('county-detail', ['county' => $menu['village']['id'], 'id' =>$item['id']])}}">{{$item['title']}}</a></h4>
						</div>
					</li>
					@endforeach
				</ul>
			</div>
			<div class="col-md-4 visible-lg visible-md cont-right wow bounceInRight">
				
				<div class="row cont-sidebar cont-sidebar-img">
					<div class="col-xs-12">
						<img class="img-responsive" src="/content/sucai/laoji.png" />
					</div>
				</div>
			</div>
		</div>
</section>

@endsection()