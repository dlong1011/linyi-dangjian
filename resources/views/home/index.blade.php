@extends('master')

@section('content')

<section class="container cont-main">
		<div class="row">
			<div class="col-sm-4 col-xs-4"  style="overflow: hidden; height: 120px;">
				<img width="100%" src="content/sucai/img-6.png" />
			</div>
			<div class="col-sm-4 col-xs-4" style="overflow: hidden; height: 120px;">
				<img width="100%" src="content/sucai/img-4.png" />
			</div>
			<div class="col-sm-4 col-xs-4" style="overflow: hidden; height: 120px;">
				<img width="100%" src="content/sucai/4576203_113523016088_2.png" />
			</div>
		</div>
	</section>
	<section class="container cont-main cont-main-list">
		<div class="row">
			<div class="col-sm-10 col-xs-10 cont-at-text" style="background-color: #eacb65;height:53px;">
				<div class="cont-at-roundup " style="padding:5px;">
				临猗古称郇阳，1954年由临晋县和猗氏县合并而成。位居黄河中游秦晋豫金三角地带北沿，地理坐标：东经110°17′30.7″---110°54′38.9″，北纬34°58′52.9″---35°18′47.6″.东西阔55公里，南北长33公里，总面积1339.32平方公里。 
				</div>
			</div>
			<div class="col-sm-2 col-xs-2 cont-at-text" style="height:53px;">
				<a href="{{URL::to('admin/auth/login')}}"><img src="/content/zcms/cont/images/login-show.png" width = "100%" /></a>
			</div>
		</div>
	</section>
	<section class="container cont-main map">
		<div class="map-left">
			<div style="width:800px;height:548px;position: absolute;">
				<img src="content/zcms/index/images/linyi.png" width="800" height="548" alt="临猗"/>
				
				<!-- <div class="1" style="position:absolute;top:4px;left:46px;width:256px;display:none;"><img width="100%" src="content/zcms/index/images/sunjizhen.png"></div>
				<div class="2" style="position:absolute;top:49px;left:258px;width:170px;display:none;"><img width="100%" src="content/zcms/index/images/beixinxiang.png"></div>
				<div class="3" style="position:absolute;top:0px;left:416px;width:218px;display:none;"><img width="100%" src="content/zcms/index/images/beijingxiang.png"></div>
				<div class="4" style="position:absolute;top:73px;right:37px;width:165px;display:none;"><img width="100%" src="content/zcms/index/images/sanguanzhen.png"></div>
				<div class="5" style="position:absolute;top:179px;right:78px;width:222px;display:none;"><img width="100%" src="content/zcms/index/images/yishizhen.png"></div>
				<div class="6" style="position:absolute;bottom:84px;right:0px;width:157px;display:none;"><img width="100%" src="content/zcms/index/images/chuhouxiang.png"></div>
				<div class="7" style="position:absolute;bottom:59px;right:115px;width:165px;display:none;"><img width="100%" src="content/zcms/index/images/niuduzhen.png"></div>
				<div class="8" style="position:absolute;bottom:94px;right:233px;width:182px;display:none;"><img width="100%" src="content/zcms/index/images/meiyangzhen.png"></div>
				<div class="9" style="position:absolute;bottom:6px;left:343px;width:160px;display:none;"><img width="100%" src="content/zcms/index/images/miaoshangxiang.png"></div>
				<div class="10" style="position:absolute;bottom:1px;left:193px;width:185px;display:none;"><img width="100%" src="content/zcms/index/images/qijizhen.png"></div>
				<div class="11" style="position:absolute;bottom:26px;left:1px;width:230px;display:none;"><img width="100%" src="content/zcms/index/images/dongzhangzhen.png"></div>
				<div class="12" style="position:absolute;bottom:171px;left:9px;width:226px;display:none;"><img width="100%" src="content/zcms/index/images/jiaobeizhen.png"></div>
				<div class="13" style="position:absolute;top:293px;left:184px;width:211px;display:none;"><img width="100%" src="content/zcms/index/images/linjinzhen.png"></div>
				<div class="14" style="position:absolute;top:140px;left:222px;width:228px;display:none;"><img width="100%" src="content/zcms/index/images/zhenzizhen.png"></div>
			 -->
			 	<div class="1" style="position:absolute;top:7px;left:54px;width:252px;display:none;"><img width="100%" src="content/zcms/index/images/sunjizhen.png"></div>
				<div class="2" style="position:absolute;top:49px;left:258px;width:170px;display:none;"><img width="100%" src="content/zcms/index/images/beixinxiang.png"></div>
				<div class="3" style="position:absolute;top:0px;left:413px;width:218px;display:none;"><img width="100%" src="content/zcms/index/images/beijingxiang.png"></div>
				<div class="4" style="position:absolute;top:78px;right:50px;width:162px;display:none;"><img width="100%" src="content/zcms/index/images/sanguanzhen.png"></div>
				<div class="5" style="position:absolute;top:177px;right:84px;width:225px;display:none;"><img width="100%" src="content/zcms/index/images/yishizhen.png"></div>
				<div class="6" style="position:absolute;bottom:84px;right:10px;width:159px;display:none;"><img width="100%" src="content/zcms/index/images/chuhouxiang.png"></div>
				<div class="7" style="position:absolute;bottom:59px;right:120px;width:169px;display:none;"><img width="100%" src="content/zcms/index/images/niuduzhen.png"></div>
				<div class="8" style="position:absolute;bottom:102px;right:233px;width:178px;display:none;"><img width="100%" src="content/zcms/index/images/meiyangzhen.png"></div>
				<div class="9" style="position:absolute;bottom:16px;left:343px;width:160px;display:none;"><img width="100%" src="content/zcms/index/images/miaoshangxiang.png"></div>
				<div class="10" style="position:absolute;bottom:10px;left:193px;width:185px;display:none;"><img width="100%" src="content/zcms/index/images/qijizhen.png"></div>
				<div class="11" style="position:absolute;bottom:36px;left:10px;width:227px;display:none;"><img width="100%" src="content/zcms/index/images/dongzhangzhen.png"></div>
				<div class="12" style="position:absolute;bottom:171px;left:19px;width:226px;display:none;"><img width="100%" src="content/zcms/index/images/jiaobeizhen.png"></div>
				<div class="13" style="position:absolute;top:285px;left:184px;width:211px;display:none;"><img width="100%" src="content/zcms/index/images/linjinzhen.png"></div>
				<div class="14" style="position:absolute;top:140px;left:222px;width:228px;display:none;"><img width="100%" src="content/zcms/index/images/zhenzizhen.png"></div>
			
			</div>
			<img src="content/zcms/index/images/linyi-bg.png" width="800" height="548" alt="临猗" usemap="#county" style="position: absolute;"/>
			<map name="county" id="county">
                @foreach ($county_list as $county)
				<area shape="poly" coords="{{$county['map_path']}}"  alt="{{$county['name']}}" onclick="{{'showCounty(' . $county['id'] . ');'}}">
				@endforeach
			</map>
		</div>
		<div class="map-right">
            @foreach ($county_list as $county)
            <div id = "{{'county' . $county['id']}}" style="{{$loop->first ? 'display:block' : 'display:none' }}">
                <div class="title"><span class=" glyphicon glyphicon-home"></span>&nbsp;&nbsp;{{$county['name']}}</div>
                <div class="county-list">
                    <ul>
                        @foreach ($county['village'] as $item)
                        <li><a href ="{{URL::route('county-index', ['county' => $item['id']])}}">{{$item['name']}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endforeach
		</div>
	</section>
	<section class="container cont-main cont-main-list">
		<div class="row">
				<img width="100%" src="content/sucai/img-5.png" />
		</div>
	</section>

@endsection()

@section('script')
@parent

<script>
	var showArea = '.1';
	$(showArea).show();
    function showCounty(id) {
		if (showArea == '.' + id) {
			return;
		}
        $('.map-right>div').hide();
        $('#county' + id).slideDown();
		$(showArea).hide();
		$('.' + id).fadeIn('fast', function(){
			$(this).animate({width:'+=3px'});
		});
		$(showArea).animate({width:'-=3px'},function(){
			$(this).fadeOut('fast');
		});	
		showArea = '.' + id; 
    }
</script>

@endsection
