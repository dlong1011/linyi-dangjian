@extends('master')

@section('content')
<section class="container cont-main">
		<ul class="breadcrumb">
			<li><a href="/">首页</a></li>
			@if (!empty($menu['town']))
			<li><a href="{{URL::route('county-list', ['county' => $menu['town']['id']])}}">{{$menu['town']['name']}}</a></li>
			@endif
			@if (!empty($menu['village']))
			<li><a href="{{URL::route('county-index', ['county' => $menu['village']['id']])}}">{{$menu['village']['name']}}</a></li>
			@endif
			<!-- <li class="active"></li> -->
		</ul>
		<div class="row cont-main-list" style="padding-top:20px;">
			<div class="col-sm-12 col-md-8 cont-left wow bounceInLeft">
				<div class="cont-text">
					<h3 style="text-align:center;">{{$article['title']}}</h3><br>
					{!! $article['content'] !!}
				</div>
			</div>
			<div class="col-md-4 visible-lg visible-md cont-right wow bounceInRight">
				<div class="row cont-sidebar cont-sidebar-img">
					<div class="col-xs-12">
						<img class="img-responsive" src="/content/sucai/laoji.png" />
					</div>
				</div>
			</div>
		</div>
</section>

@endsection()


@section('script')
@parent

<script>
    console.log(@json($article));
</script>

@endsection
