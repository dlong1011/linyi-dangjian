@extends('master')

@section('content')

<section class="container cont-main">
		<ul class="breadcrumb">
			<li><a href="/">首页</a></li>
			<!-- <li class="active"></li> -->
		</ul>
		<div class="row">
			  <div class="col-sm-12 col-xs-12 cont-at-text">
			  	  <h1>&nbsp; {{$countyItem['name']}}</h1>
				  <div class="cont-at-roundup ">
				  {{$countyItem['introductory']}}
						<a href = "{{URL::route('county-info', ['id' => $countyItem['id'], 'type' => 1])}}">阅读全文</a>
				  </div>
			  </div>
		</div>
	</section>
	<section class="container cont-main cont-main-list">
		
		<div class="row">
			<div class="col-sm-12 col-md-8 cont-left wow bounceInLeft">
				<ul class="cont-news">
					@foreach ($list as $item)
					<li class="row">
						<div class="col-sm-2 cont-at-img col-xs-12">
							<a href="{{URL::route('county-index', ['county' => $item['id']])}}"><img class="img-responsive" src="/upload/{{$item['image']}}" /></a>
						</div>
						<div class="col-sm-10 col-xs-12 cont-at-text">
							<h4><a href="{{URL::route('county-index', ['county' => $item['id']])}}">{{$item['name']}}</a></h4>
							<div class="cont-at-roundup ">
								{{$item['introductory']}}
								<a href = "{{URL::route('county-info', ['id' => $item['id'], 'type' => 2])}}">阅读全文</a>
							</div>
						</div>
					</li>
					@endforeach
				</ul>
			</div>
			<div class="col-md-4 visible-lg visible-md cont-right wow bounceInRight">
				
				<div class="row cont-sidebar cont-sidebar-img">
					<div class="col-xs-12">
						<img class="img-responsive" src="/content/sucai/laoji.png" />
					</div>
				</div>
			</div>
		</div>
</section>

@endsection()