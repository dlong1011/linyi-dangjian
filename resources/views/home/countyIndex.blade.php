@extends('master')

@section('content')

<section class="container cont-main">
	<ul class="breadcrumb">
			<li><a href="/">首页</a></li>
			<li><a href="{{URL::route('county-list', ['county' => $menu['town']['id']])}}">{{$menu['town']['name']}}</a></li>
			<!-- <li class="active"></li> -->
		</ul>
		<div class="row">
			<div class="col-sm-12 col-xs-12 cont-at-text">
			<a href = "{{URL::route('county-info', ['id' => $countyItem['id'], 'type' => 2 ])}}"><h1>{{$countyItem['name']}}</h1></a>
				<div class="cont-at-roundup ">
				&nbsp;&nbsp;{{$countyItem['introductory']}}
				<a href = "{{URL::route('county-info', ['id' => $countyItem['id'], 'type' => 2 ])}}">阅读全文</a></div>
			</div>
		</div>
	</section>
	<section class="container cont-main cont-main-list">
		<div class="row">
			@foreach ($list as $category)
			<div class="col-lg-4 col-sm-6 county-module margin-top20">
				<div class="row">
					<div class="col-xs-9 col-title"><i class="glyphicon glyphicon-flag"></i>&nbsp;&nbsp;<i>{{$category['name']}}</i></div>
					<div class="col-xs-3 col-title-more">
					<a href="{{URL::route('article-list', ['county' => $countyItem['id'] ,'id' => $category['id']])}}" class="btn btn-default btn-xs btn-more ">查看更多</a></div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<ul>
							@foreach ($category['article'] as $item)
							@if  ($loop->first)
							<li class="fir-data">
								<div class="row">
									<div class="col-xs-4">
									<div class="news-img">
										<a href="{{URL::route('county-detail', ['county' => $countyItem['id'],'id' => $item['id']])}}">
											<img src="/upload/{{$item['image_url']}}" />
										</a>
									</div>
								</div>
									<div class="col-xs-8 news-info">
										<div class="news-title"><a href="{{URL::route('county-detail', ['county' => $countyItem['id'], 'id' => $item['id']])}}">{{$item['title']}}</a></div>
										<div class="news-date"> {{$item['created_at']}}</div>
									</div>
								</div>
							</li>
							@elseif (!$loop->first)
							<li class=" li-news">
								<div class="row">
									<div class="col-sm-8 col-xs-12">
										<a href="{{URL::route('county-detail', ['county' => $countyItem['id'], 'id' => $item['id']])}}">{{$item['title']}}</a>
									</div>
									<div class="col-sm-4 hidden-xs">
										{{$item['created_at']}}
									</div>
								</div>
							</li>
							@endif
							@endforeach	
						</ul>
					</div>
				</div>
			</div>
			@if ($loop->index == 2)
			<div class="col-lg-12 col-sm-12 margin-top20">
				<img src ="/content/sucai/t1.png" width="100%" />
			</div>
			@elseif ($loop->index == 5)
			<div class="col-lg-12 col-sm-12 margin-top20">
				<img src ="/content/sucai/t2.png"  width="100%"/>
			</div>
			@elseif ($loop->index == 8)
			<div class="col-lg-12 col-sm-12 margin-top20">
				<img src ="/content/sucai/t3.png"  width="100%"/>
			</div>
			@endif
			@endforeach
		</div>
</section>

@endsection()


@section('script')
@parent

<script>
    console.log(@json($list));
</script>

@endsection