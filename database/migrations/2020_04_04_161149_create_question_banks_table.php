<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_banks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->default('')->comment("题库主题");
            $table->string('question')->default('')->comment("题目");
            $table->string('item')->default('')->comment("选项");
            $table->string('right_answers')->default('')->comment("正确答案");
            $table->unsignedTinyInteger('grade')->default(0)->comment("分数");
            $table->unsignedTinyInteger('type')->default(0)->comment("题目类型1=判断2=单选3=多选,4=投票,");
            $table->timestamps();
            $table->softDeletes();
            $table->index("name", "name_index");
            $table->engine = 'InnoDB';
        });
        $prefix = env('DB_PREFIX');
        \Illuminate\Support\Facades\DB::statement("alter table " . $prefix . "question_banks comment '题库'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_banks');
    }
}
