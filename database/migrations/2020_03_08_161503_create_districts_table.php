<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('districts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('乡/镇/村名称');
            $table->string('map_path')->nullable()->default('')->comment('乡镇地图path');
            $table->unsignedBigInteger('parent_id')->default(0)->comment('所属乡镇');
            $table->string('introductory')->nullable()->default('')->comment('村概况简介');
            $table->text('description')->comment('村概况详情');
            $table->string('image')->default('')->comment('村封面图');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('districts');
    }
}
