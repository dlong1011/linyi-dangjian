<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('visitors_id')->default('0')->comment("访客id");
            $table->string('question_banks_name')->default('')->comment("题库主题");
            $table->unsignedTinyInteger('grade')->default(0)->comment("答题总分数");
            $table->unsignedInteger('created_at')->default(0)->comment('创建时间');
            $table->unsignedInteger('updated_at')->default(0)->comment('修改时间');
            $table->unsignedInteger('deleted_at')->default(0)->comment('删除时间');
            $table->index("question_banks_name", "question_banks_name_index");
            $table->engine = 'InnoDB';
        });
        $prefix = env('DB_PREFIX');
        \Illuminate\Support\Facades\DB::statement("alter table " . $prefix . "answer_records comment '答题记录'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_records');
    }
}
