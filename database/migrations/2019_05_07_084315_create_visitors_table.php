<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('openid', 50)->nullable(false)->default('')->comment("openid");
            $table->string('unionid', 100)->nullable(false)->default('')->comment("unionid");
            $table->string('nickname', 30)->nullable(false)->default('')->comment("nickname");
            $table->tinyInteger('gender')->nullable(false)->default(0)->comment("gender0=未知 1=男 2=女");
            $table->string('city', 30)->nullable(false)->default('')->comment("city");
            $table->string('province', 30)->nullable(false)->default('')->comment("province");
            $table->string('country', 30)->nullable(false)->default('')->comment("country");
            $table->string('avatar_url', 300)->nullable(false)->default('')->comment("avatar_url");
            $table->string('appid', 32)->nullable(false)->default('')->comment("appid");
            $table->unsignedInteger('created_at')->default(0)->comment('创建时间');
            $table->unsignedInteger('updated_at')->default(0)->comment('修改时间');
            $table->unsignedInteger('deleted_at')->default(0)->comment('删除时间');
            $table->index("openid", "openid_index");
            $table->engine = 'InnoDB';
        });
        $prefix = env('DB_PREFIX');
        \Illuminate\Support\Facades\DB::statement("alter table ".$prefix."visitors comment '小程序访客'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitors');
    }
}
