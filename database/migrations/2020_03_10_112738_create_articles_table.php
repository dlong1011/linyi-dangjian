<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->comment('标题');
            $table->string('image_url')->comment('封面图');
            $table->unsignedBigInteger('district_id')->comment('所属村id');
            $table->integer('category_id')->comment('所属大类（1-10个大类）');
            $table->text('content')->comment('文章内容');
            $table->integer('admin_user_id')->comment('发布人管理员id');
            $table->tinyInteger('is_check')->comment('已审核0=否，1=是');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
