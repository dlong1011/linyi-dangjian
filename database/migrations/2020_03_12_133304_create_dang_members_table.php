<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDangMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dang_members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('姓名');
            $table->unsignedTinyInteger('gender')->comment('性别0=女,1=男');
            $table->string('duty')->comment('职务');
            $table->unsignedInteger('in_office_time')->comment('任职时间');
            $table->unsignedInteger('birthday')->comment('生日');
            $table->unsignedInteger('join_party_time')->comment('入党时间');
            $table->unsignedTinyInteger('education')->comment('学历(0=无，1=小学，依次向上)');
            $table->char('card_id',18)->comment('身份证号');
            $table->text('content')->nullable()->comment('履历内容');
            $table->unsignedBigInteger('district_id')->comment('所属行政区划');
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dang_members');
    }
}
