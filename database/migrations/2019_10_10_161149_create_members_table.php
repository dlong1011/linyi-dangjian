<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('visitors_id')->default('0')->comment("访客id");
            $table->char('name', 5)->nullable(false)->default('')->comment("姓名");
            $table->char('mobile', 11)->nullable(false)->default('')->comment("手机");
            $table->unsignedInteger('integration')->default(0)->comment("积分");
            $table->unsignedInteger('unit_id')->default(0)->comment("所属单位id");
            $table->unsignedInteger('position_id')->default(0)->comment("所属职位id");
            $table->unsignedInteger('created_at')->default(0)->comment('创建(注册)时间');
            $table->unsignedInteger('updated_at')->default(0)->comment('修改时间');
            $table->unsignedInteger('deleted_at')->default(0)->comment('删除时间');
            $table->index("visitors_id", "visitors_id_index");
            $table->engine = 'InnoDB';
        });
        $prefix = env('DB_PREFIX');
        \Illuminate\Support\Facades\DB::statement("alter table " . $prefix . "members comment '已注册正式会员表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
