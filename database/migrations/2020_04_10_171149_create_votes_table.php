<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('visitors_id')->default(0)->comment("访客id");
            $table->unsignedInteger('question_banks_id')->default(0)->comment("投票题库id");
            $table->char('item')->default('a')->comment("投票选项");
            $table->unsignedInteger('created_at')->default(0)->comment('创建时间');
            $table->unsignedInteger('updated_at')->default(0)->comment('修改时间');
            $table->unsignedInteger('deleted_at')->default(0)->comment('删除时间');
            $table->index("question_banks_id", "question_banks_id_index");
            $table->index("visitors_id", "visitors_id_index");
            $table->engine = 'InnoDB';
        });
        $prefix = env('DB_PREFIX');
        \Illuminate\Support\Facades\DB::statement("alter table " . $prefix . "votes comment '讲师投票'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
