<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class SubmitRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'visitors_id'         => 'required|integer',
            'question_banks_name' => 'required|string',
            'grade'               => 'required|integer'
        ];
    }
}
