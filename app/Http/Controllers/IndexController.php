<?php
/*
 * @Author: your name
 * @Date: 2020-03-16 14:48:48
 * @LastEditTime: 2020-03-22 17:14:33
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \linyi-dangjian\app\Http\Controllers\Home\IndexController.php
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\District;
use Illuminate\View\View;

class IndexController extends Controller
{

    public function index()
    {
        $countyList = $this->countyList();
        return view('home.index')->with('county_list', $countyList);
    }

    /**
     * 乡镇分组列表
     * @return array
     * @Author    DuanTuo
     * @DateTime  2020-03-18
     */
    private function countyList()
    {
        $town = District::query()
            ->where('parent_id', 0)
            ->get(['name','id','map_path'])
            ->take(1000)
            ->toArray();
        $village = District::query()
            ->where('parent_id', '>', 0)
            ->get(['name','id','parent_id'])
            ->take(1000)
            ->toArray();
        foreach ($town as &$itemTown) {
            $itemTown['village'] = [];
            foreach ($village as &$itemVillage) {
                if ($itemTown['id'] == $itemVillage['parent_id']) {
                    $itemTown['village'][] = $itemVillage;
                    unset($itemVillage);
                }
            }
        }
        return $town;
    }

    public function compose(View $view)
    {
        $countys = District::query()
            ->where('parent_id', 0)
            ->get(['name','id'])
            ->toArray();
        $view->with('countys', $countys);
    }
}
