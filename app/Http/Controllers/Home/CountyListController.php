<?php
/*
 * @Author: your name
 * @Date: 2020-03-16 14:48:48
 * @LastEditTime: 2020-03-20 01:08:08
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \linyi-dangjian\app\Http\Controllers\Home\IndexController.php
 */

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\District;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Request;

class CountyListController extends Controller
{
    
    public function index($county)
    {
        $countyList = District::query()
            ->where('parent_id', $county)
            ->get()
            ->toArray();
        $countyItem = District::query()
            ->where('id', $county)
            ->first(['name', 'introductory','id'])
            ->toArray();
        return view('home.countyList')->with('list', $countyList)->with('countyItem', $countyItem);
    }

}
