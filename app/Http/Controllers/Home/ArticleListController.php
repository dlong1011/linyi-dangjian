<?php
/*
 * @Author: your name
 * @Date: 2020-03-16 14:48:48
 * @LastEditTime: 2020-04-07 22:13:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \linyi-dangjian\app\Http\Controllers\Home\IndexController.php
 */

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\District;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;

class ArticleListController extends Controller
{
    
    const CATEGORY = [
        1 => '村情概况',
        2 => '党组织建设',
        3 => '人才队伍建设',
        4 => '群团组织情况',
        5 => '基础保障情况',
        6 => '产业发展情况',
        7 => '村集体经济情况',
        8 => '村级治理情况',
        9 => '精准扶贫情况',
        10 => '特色亮点',
        11 => '“两委”主干档案',
        12 => '村级荣誉',
    ];
    
    public function index($county, $id)
    {
        $articleList = Article::query()
            ->where('category_id', $id)
            ->where('district_id', $county)
            ->take(100)
            ->get()
            ->toArray();
        if (empty($articleList)) {
            Redirect::route('index');
        }
        $village = District::query()->where('id', $county)->first(['id', 'name', 'parent_id'])->toArray();
        $town = District::query()->where('id', $village['parent_id'])->first(['id', 'name'])->toArray();
        $menu = [];
        if (!empty($village) && !empty($village)) {
            $menu = [
                'town' => $town,
                'village' => $village
            ];
        }
        $type = self::CATEGORY[$id];
        return view('home.articleList')->with('list', $articleList)->with('type', $type)->with('menu', $menu);
    }

}
