<?php
/*
 * @Author: your name
 * @Date: 2020-03-16 14:48:48
 * @LastEditTime: 2020-04-07 21:00:51
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \linyi-dangjian\app\Http\Controllers\Home\IndexController.php
 */

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\District;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class CountyIndexController extends Controller
{

    const CATEGORY = [
        ['id' => 1, 'name' => '村情概况'],
        ['id' => 2, 'name' => '党组织和党员队伍建设情况'],
        ['id' => 3, 'name' => '村干部人才队伍建设情况'],
        ['id' => 4, 'name' => '群团组织和社会组织情况'],
        ['id' => 5, 'name' => '基础保障情况'],
        ['id' => 6, 'name' => '产业发展情况'],
        ['id' => 7, 'name' => '村集体经济情况'],
        ['id' => 8, 'name' => '村级治理情况'],
        ['id' => 9, 'name' => '精准扶贫情况'],
        ['id' => 10, 'name' => '特色亮点'],
        ['id' => 11, 'name' =>  '“两委”主干档案'],
        ['id' => 12, 'name' => '村级荣誉'],
    ];

    public function index($county)
    {
        $articleList = $this->articleList($county);

        $village = District::query()->where('id', $county)->first(['parent_id'])->toArray();
        $town = District::query()->where('id', $village['parent_id'])->first(['id', 'name'])->toArray();
        // 组装菜单
        $menu = [];
        if (!empty($village) && !empty($village)) {
            $menu = [
                'town' => $town,
            ];
        }
        $countyItem = District::query()
            ->where('id', $county)
            ->first(['name','introductory','id'])
            ->toArray();
        return view('home.countyIndex')->with('list', $articleList)->with('countyItem', $countyItem)->with('menu', $menu);
    }

    /**
     * 依分类分组的文章数据
     * @return array
     * @DateTime  2020-03-18
     */
    private function articleList($districtsId)
    {
        $articleList = Article::query()
            ->where('district_id', $districtsId)
            ->take(1000)
            ->get()
            ->toArray();
        $cateList = self::CATEGORY;
        foreach ($cateList as &$cate) {
            $cate['article'] = [];
            foreach ($articleList as &$itemarticle) {
                if ($itemarticle['category_id'] == $cate['id']) {
                    if (count($cate['article']) == 5) {
                        continue;
                    }
                    $itemarticle['created_at'] = Carbon::parse($itemarticle['created_at'])->format('Y-m-d');
                    $cate['article'][] = $itemarticle;
                    unset($itemarticle);
                }
            }
        }
        return $cateList;
    }
}
