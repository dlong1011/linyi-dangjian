<?php
/*
 * @Author: your name
 * @Date: 2020-03-16 14:48:48
 * @LastEditTime: 2020-03-22 19:30:19
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \linyi-dangjian\app\Http\Controllers\Home\IndexController.php
 */

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\District;
use Illuminate\Support\Facades\Redirect;

class CountyDetailController extends Controller
{
    
    public function index($id, $type)
    {
        $article = District::query()
            ->where('id', $id)
            ->first()
            ->toArray();
        if (empty($article)) {
            Redirect::route('index');
        }
        $result = [
            'title' => $article['name'],
            'content' => $article['description'],
        ];
        $village = District::query()->where('id', $id)->first(['id', 'name'])->toArray();
        $menu = [];
        $menuType = 'village';
        if ($type == 1) {
            $menuType = 'town';
        }
        if (!empty($village)) {
            $menu = [
                $menuType => $village
            ];
        }
        return view('home.countyDetail')->with('article', $result)->with('menu', $menu);
    }
}
