<?php
/*
 * @Author: your name
 * @Date: 2020-03-16 14:48:48
 * @LastEditTime: 2020-03-22 19:18:08
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \linyi-dangjian\app\Http\Controllers\Home\IndexController.php
 */

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\District;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Request;

class DetailController extends Controller
{
    
    public function index($county, $id)
    {
        $article = Article::query()
            ->where('id', $id)
            ->first()
            ->toArray();
        
        $village = District::query()->where('id', $county)->first(['id', 'name', 'parent_id'])->toArray();
        $town = District::query()->where('id', $village['parent_id'])->first(['id', 'name'])->toArray();
        $menu = [];
        if (!empty($village) && !empty($village)) {
            $menu = [
                'town' => $town,
                'village' => $village
            ];
        }
        return view('home.countyDetail')->with('article', $article)->with('menu', $menu);
    }
}
