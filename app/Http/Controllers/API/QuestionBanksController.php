<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AnswerRecord;
use App\Models\QuestionBank;
use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestionBanksController extends Controller
{
    /**
     * 获取题库
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @author: DLong
     */
    public function index(Request $request)
    {
        $params = $this->validate(
            $request,
            [
                'type'        => 'required|integer',
                'visitors_id' => 'sometimes|integer',
            ]
        );
        //最新题库
        $newName = QuestionBank::query()->orderBy('id', 'desc')->get();
        if (!$newName->count()) {
            return $this->failed('服务器异常，请联系技术员');
        }
        $question_banks_id   = $newName->where('type', 4)->first();
        $question_banks_name = $newName->where('type', '<>', 4)->first();
        if ($params['type'] == 0) {
            $count = Vote::query()
                ->where('question_banks_id', $question_banks_id->id ?? 0)
                ->where('visitors_id', $params['visitors_id'] ?? 0)->count();
            if ($count) {
                return $this->failed('每人只能投票一次');
            }
        } else {
            $count = AnswerRecord::query()
                ->where('question_banks_name', $question_banks_name->name ?? '')
                ->where('visitors_id', $params['visitors_id'] ?? 0)->count();
            if ($count) {
                return $this->failed('每人只能答题一次');
            }
        }
        $list = DB::table('question_banks')
            //->where('name', $newName->name)
            ->when($params['type'] == 0, function ($query) {
                return $query->where('type', 4);
            })
            ->when($params['type'] != 0, function ($query) {
                return $query->where('type', '<>', 4)
                    ->inRandomOrder()
                    ->take(env('RandomNums'));
            })
            ->where('deleted_at', null)
            ->get(['id', 'name', 'question', 'item', 'right_answers', 'grade', 'type'])
            ->toArray();

        if (empty($list)) {
            return $this->failed('无数据！');
        }
        foreach ($list as &$value) {
            $value->item = empty($value->item) ? [] : explode('；', $value->item);
        }
        return $this->success($list);
    }
}
