<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SubmitRequest;
use App\Models\AnswerRecord;
use App\Models\Member;
use App\Models\Position;
use App\Models\QuestionBank;
use App\Models\Unit;
use App\Models\Vote;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class QuestionController extends Controller
{
    public function index()
    {

    }

    /**
     * 交卷
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     * @author: DLong
     */
    public function submit(Request $request)
    {
        $data  = $this->validate(
            $request,
            [
                'visitors_id'         => 'required|integer',
                'question_banks_name' => 'required|string',
                'grade'               => 'required|integer'
            ],
            [
                'visitors_id.required'         => '[参数]visitors_id必填',
                'question_banks_name.required' => '[参数]question_banks_name必填',
                'grade.required'               => '[参数]grade必填',
            ]
        );
        $count = AnswerRecord::query()
            ->where('question_banks_name', $data['question_banks_name'])
            ->where('visitors_id', $data['visitors_id'])->count();
        if ($count) {
            return $this->failed('每人只能答题一次');
        }
        AnswerRecord::query()->create($data);
        return $this->success();
    }

    /**
     * 登记
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     * @author: DLong
     */
    public function register(Request $request)
    {
        $data = $this->validate($request, [
            'name'        => 'required|string',
            'password'    => 'required|string',
            'mobile'      => 'required|integer',
            'unit_id'     => 'required|integer',
            'position_id' => 'required|integer',
        ]);
        if (!preg_match(
            '/^((13[0-9])|(14[0-9])|(15[0-9])|(17[0-9])|(18[0-9])|(16[0-9])|(19[0-9]))\d{8}$/',
            $data['mobile']
        )) {
            return $this->failed('请输入正确的手机号');
        }
        $model           = Member::query()->firstOrCreate(['mobile' => $data['mobile']], $data);
        $model->password = $data['password'];
        $model->save();
        $theme     = QuestionBank::query()
            ->where('type', 4)
            ->orderBy('id', 'desc')->first(['name']);
        $tokenData = [
            'uId'   => $model->id ?? 0,
            'theme' => $theme->name ?? ''
        ];
        return $this->success($tokenData);
    }

    public function getTitle()
    {
        $theme     = QuestionBank::query()
            ->where('type', 4)
            ->orderBy('id', 'desc')->first(['name']);
        $tokenData = [
            'theme' => $theme->name ?? ''
        ];
        return $this->success($tokenData);
    }

    public function getUnitList()
    {
        $data = Unit::query()->get(['id', 'name']);
        return $this->success($data);
    }

    public function getPositionList()
    {
        $data = Position::query()->get(['id', 'name']);
        return $this->success($data);
    }

    /**
     * 投票
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @author: DLong
     */
    public function vote(Request $request)
    {
        $data  = $this->validate(
            $request,
            [
                'visitors_id'       => 'required|integer',
                'question_banks_id' => 'required|string',
                'item'              => 'required|string'
            ]
        );
        $count = Vote::query()
            ->where('question_banks_id', $data['question_banks_id'])
            ->where('visitors_id', $data['visitors_id'])->count();
        if ($count) {
            return $this->failed('每人只能投票一次');
        }
        //$itemArr    = explode(',', $data['item']);
        $itemArr    = json_decode($data['item'], true);
        $insertData = [];
        foreach ($itemArr as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $val) {
                    $insertData[] = [
                        'visitors_id'       => $data['visitors_id'],
                        'question_banks_id' => $key,
                        'item'              => $val,
                        'created_at'        => time(),
                    ];
                }
            } else {
                $insertData[] = [
                    'visitors_id'       => $data['visitors_id'],
                    'question_banks_id' => $key,
                    'item'              => $value,
                    'created_at'        => time(),
                ];
            }
        }
        Vote::query()->insert($insertData);
        return $this->success();
    }

    /**
     * 微信验证token
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     * @author: DLong
     */
    public function checkSignature(Request $request)
    {
        $params = $request->only(['signature', 'timestamp', 'echostr', 'nonce']);
        Log::info('$params', ['$params' => $params, 'timestamp' => $params['timestamp']]);
        $token  = 'dlong1011';
        $tmpArr = array($token, $params['timestamp'], $params['nonce']);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);
        Log::info('$tmpStr', ['$tmpStr' => $tmpStr]);
        if ($tmpStr == $params['signature']) {
            Log::info('signature', ['signature' => $params['signature']]);
            Log::info('echostr', ['echostr' => $params['echostr']]);
            echo $params['echostr'];
        } else {
            echo $params['echostr'];
        }
    }

    /**
     * 创建菜单
     * @param Request $request
     * @author: DLong
     */
    public function createButton(Request $request)
    {
        $buttons = [
            [
                "type"     => "media_id",
                "name"     => "党建动态",
                "media_id" => "X5tMqa6KHFUh2vva66k7r7WNQp3XiNsVxIwy1gzyXv4"
            ],
            [
                "type"     => "media_id",
                "name"     => "典型风采",
                "media_id" => "iKKJqIaHJAVsVmVqKob7s9YC3v51uszCEwWheQNIf70"
            ],
            [
                "name"       => "政策解读",
                "sub_button" => [
                    [
                        "type"     => "media_id",
                        "name"     => "在线教育",
                        "media_id" => "lW0T7A4F3gcaRjxLjYn16AzOYtDVAzU2lNWd0f6mB0A"
                    ],
                    [
                        "type"     => "media_id",
                        "name"     => "政策解读",
                        "media_id" => "L_2XsIllTa_17GqWnd35tBIeNsN4fS0d-I5WjdR0Ehg"
                    ],
                    [
                        "type" => "view",
                        "name" => "知识测试",
                        "url"  => "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxba1587fef42b396c&redirect_uri=http://exam.dugujiujian.net&scope=snsapi_userinfo&response_type=code#wechat_redirect"
                    ]
                ]
            ]
        ];
//        $buttons = [
//            [
//                "type" => "click",
//                "name" => "今日歌曲",
//                "key"  => "V1001_TODAY_MUSIC"
//            ],
//            [
//                "name"       => "菜单",
//                "sub_button" => [
//                    [
//                        "type" => "view",
//                        "name" => "搜索",
//                        "url"  => "http://www.soso.com/"
//                    ],
//                    [
//                        "type" => "view",
//                        "name" => "视频",
//                        "url"  => "http://v.qq.com/"
//                    ],
//                    [
//                        "type" => "click",
//                        "name" => "赞一下我们",
//                        "key"  => "V1001_GOOD"
//                    ],
//                ],
//            ],
//        ];
        try {
            $config = [
                'app_id'        => 'wxba1587fef42b396c',
                'secret'        => 'cd030bf98e6265a1f0e550f8b6d78f1f',

                // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
                'response_type' => 'array',

            ];
            $app    = Factory::officialAccount($config);
            $list   = $app->menu->list();
            Log::info('$list', ['error' => $list]);
            $current = $app->menu->current();
            Log::info('$current', ['error' => $current]);
            $app->menu->create($buttons);
        } catch (\Exception $e) {
            Log::info('createButtonError', ['error' => $e->getMessage()]);
        }
        return $this->success();
    }

    /**
     * 测试
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Validation\ValidationException
     * @author: DLong
     */
    public function test(Request $request)
    {
//        $data   = $this->validate(
//            $request,
//            [
//                'offset' => 'required|integer',
//                'count'  => 'required|integer'
//            ]
//        );
//        $config = [
//            'app_id'        => 'wxba1587fef42b396c',
//            'secret'        => 'cd030bf98e6265a1f0e550f8b6d78f1f',
//
//            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
//            'response_type' => 'array',
//
//        ];
//        $app    = Factory::officialAccount($config);
//        $list   = $app->material->list('news', $data['offset'], $data['count']);
//        return $this->success($list);
        $is_register = Member::query()->where('visitors_id', $user['id'] ?? 0)->count() ? 1 : 0;
        dd($is_register);
    }
}

