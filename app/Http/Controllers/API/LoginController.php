<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Member;
use App\Models\QuestionBank;
use App\Models\Visitor;
use EasyWeChat\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    /**
     *  授权登录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @author: DLong
     */
    public function index(Request $request)
    {
        $params = $this->validate(
            $request,
            [
                'code' => 'sometimes|string',
                'uId'  => 'sometimes|string',
            ]
        );
        $result = [];
        $user   = [];
        if (!empty($params['code'])) {
            $config = [
                'app_id'        => 'wxba1587fef42b396c',
                'secret'        => 'cd030bf98e6265a1f0e550f8b6d78f1f',

                // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
                'response_type' => 'array',

            ];
            $app    = Factory::officialAccount($config);
            $token  = $app->oauth->getAccessToken($params['code']);
            $result = $app->oauth->user($token)->getOriginal();
            // 校验返回结果
            if (!is_array($result)) {
                Log::error($result);
                return $this->failed('登录返回信息错误');
            }

            // 授权返回结果错误
            if (empty($result['openid'])) {
                Log::error('微信授权数据错误:' . var_export($result, true));
                return $this->failed('登录信息错误');
            }
            // 数据库录入新用户
            $user = Visitor::query()->firstOrCreate(['openid' => $result['openid']])->toArray();
        }
        if (!empty($params['uId'])) {
            // 数据库录入新用户
            $user = Visitor::query()->find($params['uId']);
        }
        $is_register = Member::query()->where('visitors_id', $user['id'] ?? 0)->count() ? 1 : 0;
        $theme       = QuestionBank::query()->orderBy('id', 'desc')->first(['name']);
        $tokenData   = [
            'uId'         => $user['id'] ?? 0,
            'is_register' => $is_register,
            'theme'       => $theme->name ?? ''
        ];


        return $this->success($tokenData);
    }

    /**
     * 手机号登录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @author: DLong
     */
    public function mobileLogin(Request $request)
    {
        $params = $this->validate(
            $request,
            [
                'mobile'   => 'required|integer',
                'password' => 'required|string',
            ]
        );
        $member = Member::query()->where('mobile', $params['mobile'])->first();
        if (!$member) {
            return $this->failed('请先注册');
        }
        if (!Hash::check($params['password'], $member->password ?? '')) {
            return $this->failed('请输入正确的用户名或密码');
        }
        $is_register = 1;
        $theme       = QuestionBank::query()
            ->where('type', 4)
            ->orderBy('id', 'desc')->first(['name']);
        $tokenData   = [
            'uId'         => $member['id'] ?? 0,
            'is_register' => $is_register,
            'theme'       => $theme->name ?? ''
        ];
        return $this->success($tokenData);
    }

    /**
     * 模拟登陆
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @author: zhaojie
     * @create: 2019/10/21 17:38
     */
    public function mock(Request $request)
    {
        // 推荐关系推送
        if (config('app.env') != 'dev') {
            die('APP_ENV IS NOT DEV');
        }
        $uId  = $request->get('id', '1');
        $info = Visitor::query()->where('id', $uId)->first();

        if (empty($info)) {
            return $this->failed('用户没找到');
        }

        $token = jwtToken([
            'openId' => $info->openId,
            'uId'    => $uId,
        ]);

        return $this->setHeaders(['Authorization' => 'Bearer ' . $token])->success([
            'id'       => $info->id,
            'nickName' => $info->nickName ?? '',
            'avatar'   => $info->avatar ?? '',
            'mobile'   => $info->mobile ?? '',
            'address'  => '',
            'token'    => $token,
        ]);
    }
}
