<?php
/*
 * @Author: your name
 * @Date: 2020-03-19 01:24:00
 * @LastEditTime: 2020-03-19 01:41:36
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \linyi-dangjian\app\Providers\ComposerServiceProvider.php
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('home.*','App\Http\Controllers\Home\IndexController');
    }
}
