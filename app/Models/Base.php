<?php
/**
 * Created by PhpStorm.
 * User: simple
 * Date: 2018/11/14
 * Time: 10:11 AM
 */

namespace App\Models;

use App\Handlers\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

abstract class Base extends Model
{
    /**
     * 启用软删除
     */
    use SoftDeletes;

    /**
     * 设置日期时间格式
     *
     * @var string
     */
    public $dateFormat = 'U';

    /**
     * 需要被转换日期时间格式的字段
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
