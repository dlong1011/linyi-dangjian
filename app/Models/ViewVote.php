<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewVote extends Model
{
    protected $table = 'view_vote';
    protected $guarded = ['id', 'created_at'];

}
