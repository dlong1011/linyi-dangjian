<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewGrade extends Model
{
    protected $table = 'view_grade';
    protected $guarded = ['id', 'created_at'];

}
