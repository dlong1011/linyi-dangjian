<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewMember extends Model
{
    protected $table = 'view_member';
    protected $guarded = ['id', 'created_at'];

}
