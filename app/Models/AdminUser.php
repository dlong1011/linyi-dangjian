<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminUser extends Model
{
    protected $fillable = ['username', 'password','phone','district_id'];

    public function district()
    {
        return $this->belongsTo(District::class);
    }

}
