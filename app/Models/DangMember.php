<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DangMember extends Model
{
    protected $guarded = ['id', 'created_at'];

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function getBirthdayAttribute($value)
    {
        return date('Y-m-d', $value);
    }

    public function getJoinPartyTimeAttribute($value)
    {
        return date('Y-m-d', $value);
    }

    public function getInOfficeTimeAttribute($value)
    {
        return date('Y-m-d', $value);
    }

}
