<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Member extends Base
{
    protected $guarded = ['id', 'created_at'];

    /**
     * 密码入库加密
     *
     * @param $value
     *
     * @return string
     * @author  maxiongfei <maxiongfei@vchangyi.com>
     * @date    2019/1/18 2:35 PM
     */
    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = Hash::make($value);
    }
}
