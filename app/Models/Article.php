<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $guarded = ['id'];

    public function author()
    {
        return $this->belongsTo(AdminUser::class,'admin_user_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

}
