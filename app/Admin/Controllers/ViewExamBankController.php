<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\ExcelImport;
use App\Models\QuestionBank;
use App\Models\ViewGrade;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ViewExamBankController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '本次考试题库';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new QuestionBank());

        $grid->column('id', __('编号'));
        $grid->column('name', __('主题'));
        $grid->column('question', __('题目'))->limit(30);
        $grid->column('item', __('选项'))->limit(30);
        $grid->column('right_answers', __('正确答案'));
        $grid->column('grade', __('分值'));
        $grid->column('type', __('类型'))->display(function ($value) {
            $text = '';
            switch ($value) {
                case 4:
                    $text = '投票';
                    break;
                case 1:
                    $text = '判断';
                    break;
                case 2:
                    $text = '单选';
                    break;
                case 3:
                    $text = '多选';
                    break;
            }
            return $text;
        });

        $grid->tools(function ($tools) {
            $tools->append(new ExcelImport());
        });

        // 全部关闭
        $grid->disableActions();
        $grid->disableExport();
        $grid->disableFilter();
        $grid->disableCreateButton();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ViewGrade::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('uname', __('Uname'));
        $show->field('pname', __('Pname'));
        $show->field('question_banks_name', __('Question banks name'));
        $show->field('grade', __('Grade'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ViewGrade());

        $form->text('name', __('Name'));
        $form->text('uname', __('Uname'));
        $form->text('pname', __('Pname'));
        $form->text('question_banks_name', __('Question banks name'));
        $form->switch('grade', __('Grade'));

        return $form;
    }
}
