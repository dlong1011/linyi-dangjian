<?php

namespace App\Admin\Controllers;

use App\Models\AdminRoleUser;
use App\Models\AdminUser;
use App\Models\District;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AdminUsersController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '用户管理';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AdminUser());
        $grid->model()->where('id', '>', 2);

        $grid->column('id', __('Id'));
        $grid->column('username', __('用户名'));
        $grid->column('district.name', __('所属乡镇'));
        $grid->column('phone', __('电话'));
        $grid->column('created_at', __('创建时间'));

        $grid->actions(function ($actions) {
            // 去掉查看
            $actions->disableView();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AdminUser::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('username', __('Username'));
        $show->field('password', __('Password'));
        $show->field('name', __('Name'));
        $show->field('district_id', __('District id'));
        $show->field('phone', __('Phone'));
        $show->field('avatar', __('Avatar'));
        $show->field('remember_token', __('Remember token'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));

        return $show;
    }

    //重写编辑
    public function edit($id, Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form(true)->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($isEditing = false)
    {
        $form = new Form(new AdminUser());
        if (!$isEditing) {
            $form->text('username', __('用户名'))->creationRules(['required', "unique:admin_users"]);
        } else {
            $form->text('username', __('用户名'))->readonly();
        }
        $form->password('password', __('密码'))->rules('required');
        $form->hidden('name')->default('普通用户');

        // 如果是编辑的情况
        if ($isEditing) {
            //编辑默认显示当前值
            $form->select('district_id', '所属乡镇')->options(function ($id) {
                $district = District::find($id);
                if ($district) {
                    return [$district->id => $district->name];
                } else {
                    return [0 => '无'];
                }
            })->rules('required')->ajax('/admin/api/districts');
        } else {
            // 定义一个名为父类目的下拉框
            $form->select('district_id', '所属乡镇')->rules('required')->ajax('/admin/api/districts');
        }
        $form->mobile('phone', __('电话'))->rules('required');

        //密码加密
        $form->saving(function (Form $form) {
            $oldPw = AdminUser::query()->where('username', $form->username)->first();
            if ($oldPw->password ?? '' != $form->password) {
                $form->password = bcrypt($form->password);
            }
        });
        //表单保存后回调
        $form->saved(function (Form $form) {
            //权限关联
            AdminRoleUser::query()->insert(['role_id' => 3, 'user_id' => $form->model()->id]);
        });
        return $form;
    }


}
