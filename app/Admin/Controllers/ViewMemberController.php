<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\ViewMemberExporter;
use App\Models\Member;
use App\Models\Position;
use App\Models\Unit;
use App\Models\ViewMember;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ViewMemberController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '已注册党员';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ViewMember());

        $grid->column('visitors_id', __('编号'));
        $grid->column('name', __('姓名'));
        $grid->column('mobile', __('手机'));
        $grid->column('uname', __('单位'));
        $grid->column('pname', __('职位'));
        //禁用
        $grid->disableCreateButton();
        //$grid->disableExport();

        $grid->actions(function ($actions) {
            // 去掉删除
            $actions->disableDelete();

            // 去掉查看
            $actions->disableView();
        });

        $grid->filter(function ($filter) {

            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            $filter->like('name', '姓名');
            $filter->like('uname', '单位');
        });
        $grid->exporter(new ViewMemberExporter());
        return $grid;
    }

    //重写编辑
    public function edit($id, Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form(true)->edit($id));
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ViewMember::findOrFail($id));

        $show->field('visitors_id', __('编号'));
        $show->field('name', __('Name'));
        $show->field('mobile', __('Mobile'));
        $show->field('uname', __('Uname'));
        $show->field('pname', __('Pname'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($isEditing = false)
    {
        if (!$isEditing) {
            return false;
        }
        $form = new Form(new Member());

        $form->text('name', __('姓名'));
        $form->mobile('mobile', __('手机'));
        // 如果是编辑的情况
        if ($isEditing) {
            //编辑默认显示当前值
            $form->select('unit_id', '单位')->options(function ($id) {
                $district = Unit::find($id);
                if ($district) {
                    return [$district->id => $district->name];
                } else {
                    return [0 => '数据异常'];
                }
            })->ajax('/admin/api/unit');
        }
        //编辑默认显示当前值
        $form->select('position_id', '职位')->options(function ($id) {
            $district = Position::find($id);
            if ($district) {
                return [$district->id => $district->name];
            } else {
                return [0 => '数据异常'];
            }
        })->ajax('/admin/api/position');

        return $form;
    }
}
