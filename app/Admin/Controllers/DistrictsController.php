<?php

namespace App\Admin\Controllers;

use App\Models\District;
use App\Models\Position;
use App\Models\Unit;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class DistrictsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '乡镇';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new District());

        $grid->column('id', __('Id'));
        $grid->column('name', __('乡镇/村名'));
        $grid->column('parent.name', __('所属乡镇'))->display(function ($value) {
            return $value ?? '无';
        });
        $grid->column('created_at', __('创建时间'));

        $grid->actions(function ($actions) {
            // 去掉查看
            $actions->disableView();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(District::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('乡镇/村名'));
        $show->field('parent.name', __('所属乡镇'));
        $show->field('introductory', __('村概况简介'));
        $show->field('description', __('村概况详情'))->unescape();
        $show->field('created_at', __('创建时间'));

        return $show;
    }

    public function edit($id, Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form(true)->edit($id));
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($isEditing = false)
    {
        $form = new Form(new District());

        $form->text('name', __('乡镇/村名'))->rules('required');
        $form->image('image', '封面图片');
        // 如果是编辑的情况
        if ($isEditing) {
            //编辑默认显示当前值
            $form->select('parent_id', '所属乡镇')->options(function ($id) {
                $district = District::find($id);
                if ($district) {
                    return [$district->id => $district->name];
                } else {
                    return [0 => '无'];
                }
            })->ajax('/admin/api/districts')->rules('required');
        } else {
            // 定义一个名为父类目的下拉框
            $form->select('parent_id', '所属乡镇')->ajax('/admin/api/districts')->rules('required');
        }
        $form->textarea('introductory', __('村概况简介'))->rules('required');
        $form->ueditor('description', __('村概况详情'))->rules('required');

        return $form;
    }

    // 定义下拉框搜索接口
    public function apiIndex(Request $request)
    {
        // 用户输入的值通过 q 参数获取
        $search = $request->input('q');
        $where  = ['parent_id', '=', 0];
        //只查村
        $search1 = $request->input('d');
        if (!empty($search1)) {
            $where = ['parent_id', '<>', 0];
        }
        $result = District::query()
            // 通过 is_directory 参数来控制
            ->where([$where])
            ->where('name', 'like', '%' . $search . '%')
            ->paginate();

        // 把查询出来的结果重新组装成 Laravel-Admin 需要的格式
        $result->setCollection($result->getCollection()->map(function (District $district) {
            return ['id' => $district->id, 'text' => $district->name];
        }));

        return $result;
    }

    // 定义下拉框搜索接口
    public function unitApi(Request $request)
    {
        // 用户输入的值通过 q 参数获取
        $search = $request->input('q');
        $result = Unit::query()
            ->where('name', 'like', '%' . $search . '%')
            ->paginate();

        // 把查询出来的结果重新组装成 Laravel-Admin 需要的格式
        $result->setCollection($result->getCollection()->map(function (Unit $district) {
            return ['id' => $district->id, 'text' => $district->name];
        }));

        return $result;
    }

    // 定义下拉框搜索接口
    public function positionApi(Request $request)
    {
        // 用户输入的值通过 q 参数获取
        $search = $request->input('q');
        $result = Position::query()
            ->where('name', 'like', '%' . $search . '%')
            ->paginate();

        // 把查询出来的结果重新组装成 Laravel-Admin 需要的格式
        $result->setCollection($result->getCollection()->map(function (Position $district) {
            return ['id' => $district->id, 'text' => $district->name];
        }));

        return $result;
    }
}
