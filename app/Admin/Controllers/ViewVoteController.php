<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\ViewVoteExporter;
use App\Models\ViewVote;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ViewVoteController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '投票结果';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ViewVote());

        $grid->column('name', __('会议主题'));
        $grid->column('vote_item', __('投票项'));
        $grid->column('vote_count', __('数量'));
        // 全部关闭
        $grid->disableActions();
        $grid->disableCreateButton();
        //$grid->disableExport();

        $grid->filter(function ($filter) {

            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            $filter->like('name', '会议主题');
        });
        $grid->exporter(new ViewVoteExporter());

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ViewVote::findOrFail($id));

        $show->field('question', __('Question'));
        $show->field('item', __('Item'));
        $show->field('vote_item', __('Vote item'));
        $show->field('vote_count', __('Vote count'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return false;
    }
}
