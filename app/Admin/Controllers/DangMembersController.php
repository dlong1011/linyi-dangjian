<?php

namespace App\Admin\Controllers;

use App\Models\AdminRoleUser;
use App\Models\DangMember;
use App\Models\District;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;

class DangMembersController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '党班子成员';


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new DangMember());

        //普通用户只能查看所在地区的党员
        if (Admin::user()->roles[0]->slug == 'user') {
            $grid->model()->where('district_id', '=', Admin::user()->district_id);
        }

        $grid->column('id', __('Id'));
        $grid->column('name', __('姓名'));
        $grid->column('gender', __('性别'))->display(function ($value) {
            return $value ? '男' : '女';
        });
        $grid->column('education', __('学历'))->display(function ($value) {
            $education = [
                0 => '无',
                1 => '小学',
                2 => '初中',
                3 => '高中',
                4 => '大专',
                5 => '本科',
                6 => '硕士',
                7 => '博士',
            ];
            return $education[$value];
        });
        $grid->column('duty', __('职务'));
        $grid->column('in_office_time', __('任职时间'));
        $grid->column('join_party_time', __('入党时间'));
        $grid->column('district.name', __('所属村（社区）'));
        $grid->column('created_at', __('创建时间'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(DangMember::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('姓名'));
        $show->field('gender', __('性别'))->as(function ($value) {
            return $value ? '男' : '女';
        });
        $show->field('birthday', __('生日'));
        $show->field('join_party_time', __('入党时间'));
        $show->field('duty', __('现任职务'));
        $show->field('in_office_time', __('任职时间'));
        $show->field('card_id', __('身份证号'));
        $show->field('education', __('学历'));
        $show->field('district_id', __('所属村'))->as(function ($value) {
            return District::query()->find($value, 'name')->name;
        });
        $show->field('content', __('履历内容'))->unescape();

        return $show;
    }


    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new DangMember());

        $education = [
            0 => '无',
            1 => '小学',
            2 => '初中',
            3 => '高中',
            4 => '大专',
            5 => '本科',
            6 => '硕士',
            7 => '博士',
        ];

        $form->text('name', __('姓名'))->rules('required');
        $form->radio('gender', __('性别'))->options(['0' => '女', '1' => '男'])->default('1');
        $form->date('birthday', __('生日'))->rules('required')->width('100%')->format('YYYY-MM-DD');
        $form->date('join_party_time', __('入党时间'))->rules('required')->width('100%')->format('YYYY-MM-DD');
        $form->text('duty', __('现任职务'))->rules('required');
        $form->date('in_office_time', __('任职时间'))->rules('required')->width('100%')->format('YYYY-MM-DD');
        $form->text('card_id', __('身份证号'))->rules('required');
        $form->select('education', __('学历'))->options($education);
        $form->ueditor('content', __('履历内容'))->rules('required');
        $form->hidden('district_id')->default(Admin::user()->district_id);

        //formb表单保存前时间处理
        $form->saving(function (Form $form) {
            $form->birthday        = strtotime($form->birthday);
            $form->join_party_time = strtotime($form->join_party_time);
            $form->in_office_time  = strtotime($form->in_office_time);
        });

        return $form;
    }
}
