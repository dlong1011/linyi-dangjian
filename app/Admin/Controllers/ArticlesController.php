<?php

namespace App\Admin\Controllers;

use App\Models\AdminUser;
use App\Models\Article;
use App\Models\District;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;

class ArticlesController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '文章';


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {

        $grid = new Grid(new Article());
        //普通用户只能查看所在地区的党员
        if (Admin::user()->roles[0]->slug == 'user') {
            $grid->model()->where('admin_user_id', '=', Admin::user()->id);
        }
        $grid->column('id', __('Id'));
        $grid->column('title', __('标题'));
        $grid->column('district.name', __('所属村'));
        $grid->column('category_id', __('所属大类'))->display(function ($value) {
            $categories = [
                1  => '村情概况',
                2  => '党组织和党员队伍建设情况',
                3  => '村干部人才队伍建设情况',
                4  => '群团组织和社会组织情况',
                5  => '基础保障情况',
                6  => '产业发展情况',
                7  => '村集体经济情况',
                8  => '村级治理情况',
                9  => '精准扶贫情况',
                10 => '特色亮点和重点工作',
                11 => '“两委”主干档案',
                12 => '村级荣誉情况',
            ];
            return $categories[$value];
        });
        $grid->column('author.username', __('发布人'));
        //$grid->column('admin_user_id', __('发布人'));
        $grid->column('created_at', __('发布时间'));

        return $grid;
    }

    //重写编辑
    public function edit($id, Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form(true)->edit($id));
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Article::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('标题'));
        $show->field('district_id', __('所属村'))->as(function ($value) {
            return District::query()->find($value, 'name')->name;
        });
        $show->field('category_id', __('所属大类'))->as(function ($value) {
            $categories = [
                1  => '村情概况',
                2  => '党组织和党员队伍建设情况',
                3  => '村干部人才队伍建设情况',
                4  => '群团组织和社会组织情况',
                5  => '基础保障情况',
                6  => '产业发展情况',
                7  => '村集体经济情况',
                8  => '村级治理情况',
                9  => '精准扶贫情况',
                10 => '特色亮点和重点工作',
                11 => '“两委”主干档案',
                12 => '村级荣誉情况',
            ];
            return $categories[$value];
        });;
        $show->field('content', __('内容'))->unescape();
        $show->field('admin_user_id', __('发布人'))->as(function ($value) {
            return AdminUser::query()->find($value, 'name')->name;
        });;
        $show->field('created_at', __('发布时间'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($isEditing = false)
    {
        $form = new Form(new Article());

        $form->text('title', __('标题'))->rules('required');
        $form->image('image_url', '封面图片');
        // 如果是编辑的情况
        if ($isEditing) {
            //编辑默认显示当前值
            $form->select('district_id', '所属村（社区）')->options(function ($id) {
                $district = District::find($id);
                if ($district) {
                    return [$district->id => $district->name];
                } else {
                    return [0 => '无'];
                }
            })->rules('required')->ajax('/admin/api/districts?d=1');
        } else {
            // 定义一个名为父类目的下拉框
            $form->select('district_id', '所属村（社区）')->rules('required')->ajax('/admin/api/districts?d=1');
        }
        $form->hidden('admin_user_id')->default(Admin::user()->id);
        $form->hidden('is_check')->default(1);
        $form->select('category_id', '大类')->options([
            1  => '村情概况',
            2  => '党组织和党员队伍建设情况',
            3  => '村干部人才队伍建设情况',
            4  => '群团组织和社会组织情况',
            5  => '基础保障情况',
            6  => '产业发展情况',
            7  => '村集体经济情况',
            8  => '村级治理情况',
            9  => '精准扶贫情况',
            10 => '特色亮点和重点工作',
            11 => '“两委”主干档案',
            12 => '村级荣誉情况',
        ])->rules('required');
        $form->ueditor('content', __('内容'))->rules('required');

        return $form;
    }
}
