<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\ViewGradeExporter;
use App\Models\ViewGrade;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ViewGradeController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = '测试成绩';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ViewGrade());

        $grid->column('id', __('编号'));
        $grid->column('name', __('姓名'))->filter('like');
        $grid->column('uname', __('单位'))->filter('like');
        $grid->column('pname', __('职位'));
        $grid->column('question_banks_name', __('会议主题'))->filter('like');
        $grid->column('grade', __('分数'));

        // 全部关闭
        $grid->disableActions();
        $grid->disableCreateButton();
        //$grid->disableExport();

        $grid->filter(function ($filter) {

            // 去掉默认的id过滤器
            $filter->disableIdFilter();
            $filter->like('question_banks_name', '会议主题');
        });
        $grid->exporter(new ViewGradeExporter());

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ViewGrade::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('uname', __('Uname'));
        $show->field('pname', __('Pname'));
        $show->field('question_banks_name', __('Question banks name'));
        $show->field('grade', __('Grade'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ViewGrade());

        $form->text('name', __('Name'));
        $form->text('uname', __('Uname'));
        $form->text('pname', __('Pname'));
        $form->text('question_banks_name', __('Question banks name'));
        $form->switch('grade', __('Grade'));

        return $form;
    }
}
