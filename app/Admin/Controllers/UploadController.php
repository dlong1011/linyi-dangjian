<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\ExcelImport;
use App\Handlers\ImageUploadHandler;
use App\Http\Controllers\Controller;
use App\Models\QuestionBank;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UploadController extends Controller
{
    /**
     * 富文本编辑器上传图片
     * @param Request $request
     * @param ImageUploadHandler $uploader
     * @return array
     */
    public function upImage(Request $request, ImageUploadHandler $uploader)
    {

//        // 初始化返回数据，默认是失败的
//        $data = [
//            'errno'   => 1,
//        ];
//        //dd($request->all());
//        // 判断是否有上传文件，并赋值给 $file
//        if ($file = $request->upload) {
//            // 保存图片到本地
//            $result = $uploader->save($request->upload, 'admin', 'editor');
//            // 图片保存成功的话
//            if ($result) {
//                $data['data'][] = $result['path'];
//                $data['errno']   = 0;
//            }
//        }

        return $result = $uploader->save($request->upload, 'admin', 'editor');;
    }

    public function excelImport(Request $request)
    {
        try {
            $file = $request->file('files')[0];
            if (empty($file) || $file->getClientOriginalExtension() != 'xlsx') {
                admin_toastr('请上传xlsx文件', 'error');
                return back();
            }
            //从第三行开始导入
            $fileList = array_slice(Excel::toArray(new ExcelImport(), $file)[0], 3);
            // 处理上传的Excel里的数据
            list($insertData, $errorInfo) = $this->dealUploadExcelData($fileList);
            if (!empty($errorInfo)) {
                admin_toastr($errorInfo, 'error');
                return back();
            }
            if (!empty($insertData)) {
                QuestionBank::query()->delete();
                // 将数据入库
                QuestionBank::query()->insert($insertData);
            }
            admin_toastr('导入成功', 'success');
        } catch (\Exception $e) {
            admin_toastr($e->getMessage(), 'error');
        }
        return back();
    }

    /**
     * 处理导入数据
     * @param $data
     * @return array
     * @author: DLong
     */
    public function dealUploadExcelData($data)
    {
        // 定义入库和错误信息数组
        $insertData = [];
        $errorInfo  = '';
        foreach ($data as $line => $item) {
            // 有空行数据，直接跳出循环
            if (empty(array_filter($item))) {
                break;
            }
            // 去除空格
            $search  = array(" ", "　", "\n", "\r", "\t");
            $replace = array("", "", "", "", "");

            if (!in_array($item[3], [1, 2, 3, 4])) {
                $errorInfo = ($line + 4) . '行题目类型不符合规则';
                break;
            }

            //不能有空值
            if (empty($item[0]) || empty($item[1])
                || empty($item[2]) || empty($item[4]) || empty($item[5])) {
                if (($item[3] == 1 && empty($item[2])) || (($item[3] == 4 && empty($item[4])))) {

                } else {
                    $errorInfo = ($line + 4) . '行不能有空值或0';
                    break;
                }
            }

            // 添加数组
            $insertData[] = [
                'name'          => str_replace($search, $replace, $item[0]),
                'question'      => str_replace($search, $replace, $item[1]),
                'item'          => str_replace($search, $replace, $item[2]),
                'type'          => str_replace($search, $replace, $item[3]),
                'right_answers' => str_replace($search, $replace, $item[4]),
                'grade'         => str_replace($search, $replace, $item[5])
            ];

        }
        return [$insertData, $errorInfo];
    }
}
