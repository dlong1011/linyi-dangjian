<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/16 0016
 * Time: 上午 10:09
 */

namespace App\Admin\Extensions;


use App\Models\ViewVote;
use Encore\Admin\Grid\Exporters\ExcelExporter;
use Maatwebsite\Excel\Concerns\FromQuery;

class ViewVoteExporter extends ExcelExporter //implements FromQuery
{

    protected $fileName = '投票结果.xlsx';

    protected $columns = [
        'name'       => '会议主题',
        'vote_item'  => '投票项',
        'vote_count' => '数量'
    ];

//    public function query()
//    {
//        return ViewVote::query()->orderBy('id', 'desc')
//            ->orderBy('vote_count', 'desc')
//            ->select(['name', 'vote_item', 'vote_count']);
//    }
}
