<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/13 0013
 * Time: 下午 7:02
 */
namespace  App\Admin\Extensions;
use Encore\Admin\Form\Field;
class Ueditor extends Field
{
    protected static $css = [
    ];
    public static $isJs=false;
    protected static $js = [
        /*ueditor1433文件夹为第二步中自定义的文件夹*/
        'vendor/laravel-admin-ext/ueditor/ueditor.config.js',
        'vendor/laravel-admin-ext/ueditor/ueditor.all.js',
    ];
    protected $view = 'admin.Ueditor';
    public function render()
    {
        $this->script = <<<EOT
        UE.delEditor('{$this->id}');
             var  ue = UE.getEditor('{$this->id}');
              
EOT;
        return parent::render();
    }

}
