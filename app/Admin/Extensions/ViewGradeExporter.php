<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/16 0016
 * Time: 上午 10:09
 */

namespace App\Admin\Extensions;


use App\Models\ViewGrade;
use Encore\Admin\Grid\Exporters\ExcelExporter;
use Maatwebsite\Excel\Concerns\FromQuery;

class ViewGradeExporter extends ExcelExporter //implements FromQuery
{
    protected $fileName = '测试成绩.xlsx';

    protected $columns = [
        'id'                  => '编号',
        'name'                => '姓名',
        'uname'               => '单位',
        'pname'               => '职位',
        'question_banks_name' => '会议主题',
        'grade'               => '分数',
    ];

//    public function query()
//    {
//        return ViewGrade::query()->orderBy('grade', 'desc')
//            ->select(['id', 'name', 'uname', 'pname', 'question_banks_name', 'grade']);
//    }
}
