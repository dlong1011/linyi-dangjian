<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/4/16 0016
 * Time: 上午 10:09
 */

namespace App\Admin\Extensions;


use Encore\Admin\Grid\Exporters\ExcelExporter;

class ViewMemberExporter extends ExcelExporter
{
    protected $fileName = '已注册党员.xlsx';

    protected $columns = [
        'visitors_id' => '编号',
        'name'        => '姓名',
        'mobile'      => '手机',
        'uname'       => '单位',
        'pname'       => '职位',
    ];
}
