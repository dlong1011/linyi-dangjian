<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'     => config('admin.route.prefix'),
    'namespace'  => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');

    $router->resource('districts', DistrictsController::class);
    $router->get('api/districts', 'DistrictsController@apiIndex');
    $router->get('api/unit', 'DistrictsController@unitApi');
    $router->get('api/position', 'DistrictsController@positionApi');

    $router->post('upload', 'UploadController@upImage');

    $router->resource('articles', ArticlesController::class);

    $router->resource('admin-users', AdminUsersController::class);

    $router->resource('dang-members', DangMembersController::class);

    $router->resource('view-members', ViewMemberController::class);

    $router->resource('view-grades', ViewGradeController::class);

    $router->resource('view-votes', ViewVoteController::class);

    $router->resource('view-banks', ViewExamBankController::class);

    $router->any('cardnewsimport', 'UploadController@excelImport');
});
