<?php

namespace App\Exceptions;

use App\Http\Traits\ApiResponse;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{

    use ApiResponse;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];


    /**
     * Report or log an exception.
     *
     * @param  \Exception $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     *
     *
     * @param \Illuminate\Http\Request $request
     * @param Exception $exception
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @author  maxiongfei <maxiongfei@vchangyi.com>
     * @date    2019/1/10 5:04 PM
     */
    public function render($request, Exception $exception)
    {
        //参数验证错误的异常，我们需要返回400 的http code  和一句错误信息
        if ($exception instanceof ValidationException) {
            return $this->setHttpCode(400)->failed($exception->errors(), 400);
        }

        //用户认证的异常，我们需要返回401的 http code 和错误信息
        if ($exception instanceof UnauthorizedHttpException) {
            return $this->setHttpCode(401)->failed($exception->getMessage(), 401);
        }

        //http错误，返回404 错误
        if ($exception instanceof HttpException) {
            return $this->setHttpCode(404)->failed($exception->getMessage(), 404);
        }

        //基础错误
        if ($exception instanceof Exception) {
            return $this->setHttpCode()->failed($exception->getMessage(), $exception->getCode());
        }
        //uc错误
        if ($exception instanceof \Error) {
            return $this->setHttpCode(500)->failed($exception->getMessage(), $exception->getCode());
        }

        return parent::render($request, $exception);
    }
}
