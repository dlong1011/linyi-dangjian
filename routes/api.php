<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->namespace('Api')->name('api.v1.')->group(function () {
    Route::get('version', function () {
        return 'this is version v1';
    })->name('version');
    //获取题库
    Route::get('get-question', 'QuestionBanksController@index');
    //微信授权登录
    Route::post('login', 'LoginController@index');
    //手机号登录
    Route::post('mobile-login', 'LoginController@mobileLogin');
    //模拟登陆
    Route::post('mock', 'LoginController@mock');

    Route::post('exam-submit', 'QuestionController@submit');
    Route::post('register', 'QuestionController@register');
    Route::get('get-title', 'QuestionController@getTitle');
    Route::get('unit-list', 'QuestionController@getUnitList');
    Route::get('position-list', 'QuestionController@getPositionList');
    //投票
    Route::post('vote-submit', 'QuestionController@vote');
    //微信验证token
    Route::get('check-token', 'QuestionController@checkSignature');
    //创建菜单
    Route::post('create-button', 'QuestionController@createButton');
    //测试
    Route::get('test', 'QuestionController@test');
});
