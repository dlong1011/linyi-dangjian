<?php
/*
 * @Author: your name
 * @Date: 2020-03-16 16:52:17
 * @LastEditTime: 2020-03-22 19:31:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \linyi-dangjian\routes\web.php
 */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/',['uses' => 'IndexController@index']);

Route::group(['namespace' => 'Home'], function () {
    Route::get('test', ['as' => 'index-test', 'uses' => 'IndexController@index']);
    Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);
    Route::get('county/index/{county}', ['as' => 'county-index', 'uses' => 'CountyIndexController@index']);
    Route::get('county/list/{county}', ['as' => 'county-list', 'uses' => 'CountyListController@index']);
    Route::get('detail/{county}/{id}', ['as' => 'county-detail', 'uses' => 'DetailController@index']);
    Route::get('county/detail/{id}/{type}', ['as' => 'county-info', 'uses' => 'CountyDetailController@index']);
    Route::get('article/{county}/{id}', ['as' => 'article-list', 'uses' => 'ArticleListController@index']);
});
